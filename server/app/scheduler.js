'use strict';

const Cron = require('cron');

let alarms = [];

module.exports = {
	setSchedule(times, callback) {
		console.log(new Date(), '[Scheduler]', 'setSchedule');

		alarms = times.map(
			(alarmTime) => {
				return new Cron.CronJob(
					alarmTime.cron,
					callback,
					null,
					true,
					'Africa/Johannesburg',
				);
			});
	},

	clearSchedule() {
		console.log(new Date(), '[Scheduler]', 'clearSchedule');

		alarms.forEach(
			(cronJob) => {
				cronJob.stop();
			});

		alarms = [];
	}
};
