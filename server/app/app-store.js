'use strict';

const Utilities = require('util');
const FileSystem = require('fs');

const { Time } = require('adhan-clock-common');

const AudioPlayer = require('./audio/audio-player');
const AudioPlayerEvent = require('./audio/audio-player-event');
const Scheduler = require('./scheduler');

const readFile = Utilities.promisify(FileSystem.readFile);
const writeFile = Utilities.promisify(FileSystem.writeFile);

let times = [
	{
		"name": "Fajr",
		"hours": 4,
		"minutes": 50
	},
	{
		"name": "Dhuhr",
		"hours": 12,
		"minutes": 30
	},
	{
		"name": "Jumuah",
		"hours": 12,
		"minutes": 30
	},
	{
		"name": "Asr",
		"hours": 16,
		"minutes": 30
	},
	{
		"name": "Maghrib",
		"hours": 18,
		"minutes": 30
	},
	{
		"name": "Isha",
		"hours": 19,
		"minutes": 9
	}
];

let lastUpdate = new Date();
let alarmIsActive = true;
let alarmIsPlaying = false;

const FILE_PATH = 'storage/data.json';

const audioPlayer = new AudioPlayer(process.env.SYSTEM_AUDIO_PLAYER);

const AppStore = {
	async loadDataFile() {
		console.log(new Date(), '[AppStore]', 'loadDataFile');

		const dataFile = await readFile(FILE_PATH);
		const data = JSON.parse(dataFile);
		lastUpdate = new Date(data.lastUpdate);

		this.setTimes(data.times);
	},

	async saveDataFile() {
		console.log(new Date(), '[AppStore]', 'saveDataFile');

		lastUpdate = new Date();
		const fileData = {
			times: this.times,
			lastUpdate: lastUpdate.toISOString(),
		}

		const dataFile = await writeFile(FILE_PATH, JSON.stringify(fileData));
	},

	setTimes(value) {
		console.log(new Date(), '[AppStore]', 'setTimes');

		times = value.map((t) => {
			return new Time(t.name, t.hours, t.minutes);
		});

		this.saveDataFile();

		this.scheduleAlarms();
	},

	soundAlarm() {
		if (!alarmIsPlaying) {
			audioPlayer.play(process.env.AUDIO_PATH);
		}
	},

	stopAlarm() {
		audioPlayer.stop();
	},

	scheduleAlarms() {
		Scheduler.clearSchedule();

		if (alarmIsActive) {
			Scheduler.setSchedule(times, () => {
				audioPlayer.play(process.env.AUDIO_PATH, '-s');
			});
		}
	},

	get data() {
		return {
			times,
			lastUpdate
		}
	},

	get times() {
		return times;
	},

	get lastUpdate() {
		return lastUpdate;
	},

	get alarmIsPlaying() {
		return alarmIsPlaying;
	},

	get alarmIsActive() {
		return alarmIsActive;
	},

	set alarmIsActive(value) {
		if (value === true || value === false) {
			alarmIsActive = value;

			this.scheduleAlarms();
		}
	}
};

audioPlayer.on(AudioPlayerEvent.PLAYING, (data) => { alarmIsPlaying = true; });
audioPlayer.on(AudioPlayerEvent.STOPPED, () => { alarmIsPlaying = false });
audioPlayer.on(AudioPlayerEvent.ERROR, () => { alarmIsPlaying = false });

module.exports = { AppStore, audioPlayer };