'use strict';

const ChildProcess = require("child_process");
const EventEmitter = require('events');
const Utils = require('events');

const { PLAYING, STOPPED, ERROR } = require('./audio-player-event');

class AudioPlayer extends EventEmitter {

	constructor(systemAudioPlayer) {
		super();
		this._systemAudioPlayer = systemAudioPlayer;
		this.audio = null;
	}

	play(filePath, ...params) {
		this.audio = ChildProcess.spawn(this._systemAudioPlayer, [filePath, ...params], { detached: false });

		this.audio.stdout.on('data', (data) => {
			console.log(new Date(), '[AudioPlayer]', PLAYING);
			this.emit(PLAYING, data);
		});

		this.audio.stdout.on('end', () => {
			console.log(new Date(), '[AudioPlayer]', STOPPED);
			this.audio = null;
			this.emit(STOPPED);
		});

		this.audio.stderr.on('data', (error) => {
			console.error(new Date(), '[AudioPlayer]', ERROR, 'error:', error);
			this.audio = null;
			this.emit(ERROR, error);
		});
	}

	stop() {
		if (this.audio) {
			this.audio.kill();
			this.audio = null;
		}

		this.emit(STOPPED);
	}
}

module.exports = AudioPlayer;