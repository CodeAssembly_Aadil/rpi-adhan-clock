'use strict';

module.exports = Object.freeze({
	PLAYING: 'AudioPlayerEvent.PLAYING',
	STOPPED: 'AudioPlayreEvent.STOPPED',
	ERROR: 'AudioPlayerEvent.ERROR'
});