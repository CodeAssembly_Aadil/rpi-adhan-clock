const SocketIO = require('socket.io');

const { ControlEvent, Time } = require('adhan-clock-common');

const { AppStore, audioPlayer } = require('./app-store');
const AudioPlayerEvent = require('./audio/audio-player-event');
const Scheduler = require('./scheduler');

function WebSocket(httpServer) {

	const server = SocketIO(httpServer, {
		path: '/control',
		serveClient: false,
		// below are engine.IO options
		pingInterval: 10000,
		pingTimeout: 5000,
		cookie: false,
		// origins: '*',
	});

	server.on('connection', (socket) => {

		const onAudioStopped = () => {
			console.log(new Date(), '[WebSocket]', ControlEvent.ALARM_IS_PLAYING, AppStore.alarmIsPlaying);
			server.emit(ControlEvent.ALARM_IS_PLAYING, AppStore.alarmIsPlaying);
		}

		audioPlayer.on(AudioPlayerEvent.PLAYING, (data) => {
			console.log(new Date(), '[WebSocket]', ControlEvent.ALARM_IS_PLAYING, AppStore.alarmIsPlaying);
			server.emit(ControlEvent.ALARM_IS_PLAYING, AppStore.alarmIsPlaying);
		});

		audioPlayer.on(AudioPlayerEvent.STOPPED, onAudioStopped);
		audioPlayer.on(AudioPlayerEvent.ERROR, onAudioStopped);

		socket.on(ControlEvent.GET_ALARM_STATE, () => {
			console.log(new Date(), '[WebSocket]', ControlEvent.SET_ALARM_STATE);
			socket.emit(ControlEvent.GET_ALARM_STATE, AppStore.alarmIsActive);
			socket.emit(ControlEvent.ALARM_IS_PLAYING, AppStore.alarmIsPlaying);
		});

		socket.on(ControlEvent.SET_ALARM_STATE, (alarmState) => {
			console.log(new Date(), '[WebSocket]', ControlEvent.SET_ALARM_STATE, alarmState);
			AppStore.alarmIsActive = alarmState;
			server.emit(ControlEvent.GET_ALARM_STATE, AppStore.alarmIsActive);
		});

		socket.on(ControlEvent.STOP_ALARM, () => {
			console.log(new Date(), '[WebSocket]', ControlEvent.STOP_ALARM);
			AppStore.stopAlarm();
		});

		socket.on(ControlEvent.GET_TIMES, () => {
			console.log(new Date(), '[WebSocket]', ControlEvent.GET_TIMES);
			socket.emit(ControlEvent.GET_TIMES, AppStore.data);
		});

		socket.on(ControlEvent.SET_TIMES, async (times) => {
			console.log(new Date(), '[WebSocket]', ControlEvent.SET_TIMES, times);
			AppStore.setTimes(times);
			server.emit(ControlEvent.GET_TIMES, AppStore.data);
		});
	});
}

module.exports = WebSocket;