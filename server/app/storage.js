'use strict';

const Utilities = require('util');
const FileSystem = require('fs');

const { Time } = require('adhan-clock-common');

const readFile = Utilities.promisify(FileSystem.readFile);
const writeFile = Utilities.promisify(FileSystem.writeFile);

function parseDataFile(dataFile) {
    const data = JSON.parse(dataFile);

    data.times = data.times.map((time) => {
        return new Time(time.name, time.hours, time.minutes);
    });

    data.lastUpdate = new Date(data.lastUpdate);

    return data;
}

async function readDataFile() {
    const dataFile = await readFile('./data.json');
    const data = JSON.parse(dataFile);

    data.times = data.times.map((time) => {
        return new Time(time.name, time.hours, time.minutes);
    });

    data.lastUpdate = new Date(data.lastUpdate);

    return data;
}

async function writeDataFile(times) {
    const lastUpdate = new Date();

    const fileData = {
        times,
        lastUpdate: lastUpdate.toISOString(),
    }

    const dataFile = await writeFile('data.json', JSON.stringify(fileData));

    return fileData;
}

module.exports = {
    parseDataFile,
    readDataFile,
    writeDataFile,
}