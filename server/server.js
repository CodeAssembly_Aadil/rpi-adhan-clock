'use strict'

const HTTP2 = require('http2');
const FileSystem = require('fs');
const Path = require('path');

const DotENV = require('dotenv');
// Load Enviroment Variables
DotENV.config();

const Koa = require('koa');
const KoaStatic = require('koa-static');
const KoaRouter = require('koa-router');

const { AppStore, audioPlayer } = require('./app/app-store');
const AudioPlayer = require('./app/audio/audio-player');
const WebSocket = require('./app/web-socket');

// Load Saved Data
AppStore.loadDataFile();

// Initialize koa and Middleware
const app = new Koa();
const router = new KoaRouter();

router.get('/beep', (ctx, next) => {
	audioPlayer.play('./assets/beep.mp3', '-s');
	ctx.body = 'Beep!';
});

app.use(router.routes())
	.use(router.allowedMethods())
	.use(KoaStatic(__dirname + '/public'));

// Initlize Server and WebSockets

const serverOptions = {
	key: FileSystem.readFileSync('./ssl/server.key'),
	cert: FileSystem.readFileSync('./ssl/server.crt'),
	allowHTTP1: true
};

const serverSecure = HTTP2.createSecureServer(serverOptions, app.callback());

WebSocket(serverSecure);

serverSecure.listen({ host: process.env.HOST, port: process.env.PORT },
	() => {
		const addressInfo = serverSecure.address();
		console.info(`${new Date()} ${process.env.NODE_ENV} Server listening on: https://${addressInfo.address}:${addressInfo.port}`);
	});
