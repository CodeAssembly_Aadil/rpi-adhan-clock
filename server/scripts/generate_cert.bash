#!/bin/bash

# Certificate details; replace items in angle brackets with your own info
SUBJECT="
/
/C=ZA\
/ST=Gauteng\
/L=Centurion\
/O=Code Assembly Pty Ltd\
/CN=adhan.local\
/emailAddress=aadil@codeassembly.co.za\
"

# Generate a Private Key
export PASSPHRASE=$(head -c 512 /dev/urandom | tr -dc a-z0-9A-Z | head -c 128; echo)

# Generate the server private key
openssl genrsa -aes256 -out ../ssl/server.pem -passout env:PASSPHRASE 4096

# Generate a CSR (Certificate Signing Request)
openssl req -new -batch -subj "$(echo -n $SUBJECT)" -key ../ssl/server.pem -out ../ssl/server.csr -passin env:PASSPHRASE

# Remove Passphrase from Key
cp ../ssl/server.pem ../ssl/server.key
openssl rsa -in ../ssl/server.key -out ../ssl/server.key -passin env:PASSPHRASE

# Generating a Self-Signed Certificate (365 days)
openssl x509 -req -days 365 -in ../ssl/server.csr -signkey ../ssl/server.key -out ../ssl/server.crt