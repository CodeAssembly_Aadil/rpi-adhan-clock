import sys

from pygame import mixer

def main():
    # Read in command
    filePath = input()

    # output alarm started
    sys.stdout.write('PLAY: ' + filePath)
    sys.stdout.close()

	# play audio
    mixer.init(frequency=44100, size=-16, channels=2, buffer=4096)
    mixer.music.load(filePath)
    mixer.music.play()
    # wait for sound to complete
    while mixer.music.get_busy():
        continue

    sys.exit(0)

# start process
if __name__ == '__main__':
    main()
