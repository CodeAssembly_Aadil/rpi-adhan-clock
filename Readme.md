# Raspberry Pi - Adhan Clock

Adhan alarm clock designed to run on a localy connected Raspberry Pi. Powered by NodeJS and built on top of Koa, SocketIO and Vue. 

The project is divided into three folders. 
 1. common: A common repo with constants and shared classes, 
 2. app: Vue powered Progressive Web App
 3. server: Koa and SocketIO powered service to store, retreive and sound alarms, also makes use of a python script.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

You will need to install the following enviromental dependencies

[Python 3](https://www.python.org/)

[Node 8.9+](https://nodejs.org) 

### Development Setup

Navigate to the ```./server``` folder and run 

```
npm i
```

Navigate to the ```./app``` folder and run 
```
npm i
```

## Deployment

Configure the ```.env``` file for the __server ip__ and __port__ you wish to deploy to.

Run the ``./server/scripts/generate_cert.bash`` to create an ssl certificate

## Built With

* [NodeJS](https://nodejs.org) - Application Runtime
* [Koa](https://http://koajs.com) - Web framework
* [SocketIO](https://socket.io) - Web Socket Libray
* [Vue](https://vuejs.org) - Frontend Framework

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

A lie eveything is in master

## Authors

* **Aadil Cachalia** - *Initial work* - [Code Assembly](https://codessembly.co.za)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone who's code was used