module.exports = {
	root: true,
	'extends': [
		'plugin:vue/recommended',
	],

	rules: {
		"indent": ["warn", "tab"],

		"semi": ["error", "always"],

		"vue/max-attributes-per-line": ["ignore", {
			"singleline": 3,
			"multiline": {
				"max": 1,
				"allowFirstLine": false
			}
		}],

		"vue/html-indent": ["error", "tab", {
			"attribute": 1,
			"closeBracket": 0,
			"ignores": []
		}],
	},
}