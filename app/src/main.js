import Vue from "vue";
import AdhanClock from "./AdhanClock.vue";

Vue.config.productionTip = false;

new Vue({
	render: h => h(AdhanClock)
}).$mount("#adhan-clock");
