module.exports = {
	lintOnSave: true,
	devServer: {
		proxy: {
			'/control': {
				target: 'https://127.0.0.1:8090/control',
				ws: true,
			},
		}
	}
};
