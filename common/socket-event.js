const CONNECT = 'connect';
const CONNECT_ERROR = 'connect_error';
const CONNECT_TIMEOUT = 'connect_timeout';
const ERROR = 'error';
const DISCONNECT = 'disconnect';
const RECONNECT = 'reconnect';
const RECONNECT_ATTEMPT = 'reconnect_attempt';
const RECONNECTING = 'reconnecting';
const RECONNECT_ERROR = 'reconnect_error';
const RECONNECT_FAILED = 'reconnect_failed';
const PING = 'ping';
const PONG = 'pong';

module.exports = {
    CONNECT,
    CONNECT_ERROR,
    CONNECT_TIMEOUT,
    ERROR,
    DISCONNECT,
    RECONNECT,
    RECONNECT_ATTEMPT,
    RECONNECTING,
    RECONNECT_ERROR,
    RECONNECT_FAILED,
    PING,
    PONG,
};
