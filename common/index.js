'use strict';

const ControlEvent = require('./control-event');
const PrayerName = require('./prayer-name');
const SocketEvent = require('./socket-event');
const Time = require('./time');
const WeekDayIndex = require('./week-day-index');

module.exports = {
    ControlEvent,
    PrayerName,
    SocketEvent,
    Time,
    WeekDayIndex,
};
