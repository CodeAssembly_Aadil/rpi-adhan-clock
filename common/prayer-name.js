'use strict';

const FAJR = 'Fajr';
const DHUHR = 'Dhuhr';
const JUMUAH = 'Jumuah';
const ASR = 'Asr';
const MAGHRIB = 'Maghrib';
const ISHA = 'Isha';

module.exports = {
    FAJR,
    DHUHR,
    JUMUAH,
    ASR,
    MAGHRIB,
    ISHA
};
