'use strict';

class Time {
    constructor(name, hours = 0, minutes = 0) {
        let _hours = 0;
        let _minutes = 0;

        Object.defineProperties(this, {
            name: {
                configurable: false,
                enumerable: true,
                writable: false,
                value: name,
            },

            hours: {
                configurable: false,
                enumerable: true,
                get: () => _hours,
                set: (value) => {
                    if (value === parseInt(value, 10) && value >= 0 && value <= 23) {
                        _hours = value;
                    }
                },
            },
            minutes: {
                configurable: false,
                enumerable: true,
                get: () => _minutes,
                set: (value) => {
                    if (value === parseInt(value, 10) && value >= 0 && value <= 59) {
                        _minutes = value;
                    }
                },
            },
        });

        Object.seal(this);

        this.hours = hours;
        this.minutes = minutes;
    }

    inSeconds() {
        return (this.hours * 60 + this.minutes) * 60;
    }

    clone() {
        return new Time(this.name, this.hours, this.minutes);
    }

    equals(time) {
        if (!(time instanceof Time)) {
            return false;
        }

        if (this === time) {
            return true;
        }

        if (this.name === time.name && this.hours === time.hours && this.minutes === time.minutes) {
            return true;
        }

        return false;
    }

    get formatted() {
        const hours = this.hours.toString().padStart(2, '0');
        const minutes = this.minutes.toString().padStart(2, '0');
        return `${hours}:${minutes}`;
    }

    get cron() {
        return `0 ${this.minutes} ${this.hours} * * *`;
    }
}


module.exports = Time;
